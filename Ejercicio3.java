package model1;

public class Ejercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Tecla escape\t\t\t Significado");
		System.out.println("\\n \t\t\t\t Significa nueva linea");
		System.out.println("\\t \t\t\t\t Significa un tab de espacio");
		System.out.println("\\\"\t\t\t\t Es para poner \" (comillas dobles) dentro de un texto, por ejemplo: \" Belencita\".");
		System.out.println("\\\\ \t\t\t\t Se utiliza para escribir la \\ dentro del texto, por ejemplo: \\Algo\\ ");
		System.out.println("\\´ \t\t\t\t Se utiliza para poner las ‘(comilla simple) en el texto, por ejemplo ‘Princesita’.");
	}

}
